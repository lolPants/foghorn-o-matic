# 📯 Foghorn-o-Matic
*Because being able to hear a conversation is overrated.*

## Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Setup
1. Rename `example.env` to `foghorn.env`
2. Fill out the bot token and bot prefix in `foghorn.env`
3. `docker-compose up -d`
