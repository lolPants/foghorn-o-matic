/**
 * Splits a string of arguments into an array. Takes into account " quote marks.
 * @param {string} args Argument String
 * @param {string} [split='"'] Character to break on. Default is (")
 * @returns {string[]} Argument Array
 */
const argumentSplitter = (args, split = '"') => {
  let arr = args.split(' ')

  let newArr = []
  let multi = false
  for (let arg of arr) {
    if (multi !== true) {
      if (arg[0] === split && arg[arg.length - 1] !== split) {
        multi = true
        newArr.push(arg.slice(1))
      } else if (arg[0] === split && arg[arg.length - 1] === split) {
        newArr.push(arg.slice(1, -1))
      } else {
        newArr.push(arg)
      }
    } else {
      let str
      if (arg.slice(-1) === split) {
        multi = false
        str = arg.slice(0, -1)
      } else {
        str = arg
      }
      newArr[newArr.length - 1] += ` ${str}`
    }
  }
  if (newArr.length === 1 && newArr[0] === '') newArr = []
  return newArr
}

/**
 * @typedef {Object} CommandPrefix
 * @property {string} prefix
 * @property {string} name
 * @property {string} arguments
 */

/**
 * Splits an input command string to Prefix and Arguments
 * @param {string} prefix Command Prefix
 * @param {string} str Input String
 * @param {boolean} [ignoreSpacing=false] Ignore Prefix Spacing
 * @returns {CommandPrefix|boolean}
 */
const prefixHandler = (prefix, str, ignoreSpacing = false) => {
  if (ignoreSpacing) return unspacedPrefixHandler(prefix, str)
  else return spacedPrefixHandler(prefix, str)
}

/**
 * Splits an input command string to Prefix and Arguments (SPACED PREFIX)
 * @param {string} prefix Command Prefix
 * @param {string} str Input String
 * @returns {CommandPrefix|boolean}
 */
const spacedPrefixHandler = (prefix, str) => {
  // Lowercase Prefix (this is important later)
  prefix = prefix.toLowerCase()

  // Split to Array
  let prefixSplit = prefix.split(' ')
  let strSplit = str.toLowerCase().split(' ')

  // Convert to Test
  let prefixTest = prefixSplit.toString()
  let strTest = strSplit.slice(0, prefixSplit.length).toString()

  // Get Args String and Return
  // The filter removes blank spaces in the array
  let name = strSplit.slice(prefixSplit.length)[0]
  let args = str.split(' ')
    .filter(a => a !== '')
    .slice(prefixSplit.length + 1)
    .join(' ')

  // Return if successful
  return prefixTest === strTest ? { prefix, name, arguments: args } : false
}

/**
 * Splits an input command string to Prefix and Arguments (UNSPACED PREFIX)
 * @param {string} prefix Command Prefix
 * @param {string} str Input String
 * @returns {CommandPrefix|boolean}
 */
const unspacedPrefixHandler = (prefix, str) => {
  // Lowercase Prefix (this is important later)
  prefix = prefix.toLowerCase()

  // Slice the string to test a prefix
  let prefixTest = str.toLowerCase().slice(0, prefix.length)

  // Split the rest of the string into an array
  // The filter removes blank spaces in the array
  let sub = str.slice(prefix.length).split(' ').filter(a => a !== '')
  let name = sub[0]
  let args = sub.slice(1).join(' ')

  // Return if successful
  return prefixTest === prefix ? { prefix, name, arguments: args } : false
}

module.exports = {
  argumentSplitter,
  prefixHandler,
}
