const { Client, Message } = require('discord.js')  // eslint-disable-line
const Command = require('../Command')
const { tempMessage } = require('../helpers')

module.exports = class Disconnect extends Command {
  constructor () {
    super('disconnect', { owner: true })
  }

  /**
   * @param {Client} client Bot Client
   * @param {Message} message Instantiating Message
   * @param {string[]} args Command Arguments
   */
  async run (client, message) {
    await tempMessage(message.channel, 'Disconnecting from all voice channels...', 5, message)
    for (let connection of client.voiceConnections.values()) {
      try {
        connection.disconnect()
      } catch (err) {
        // Ignore
      }
    }
  }
}
