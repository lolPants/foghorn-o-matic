const { Client, Message } = require('discord.js')  // eslint-disable-line
const Command = require('../Command')
const { tempMessage } = require('../helpers')

module.exports = class Leave extends Command {
  constructor () {
    super('leave', { guildOnly: true, admin: true })
  }

  /**
   * @param {Client} client Bot Client
   * @param {Message} message Instantiating Message
   * @param {string[]} args Command Arguments
   * @returns {*}
   */
  async run (client, message) {
    let voiceChannel = message.member.voice.channel
    if (!voiceChannel) {
      await tempMessage(message.channel, `${message.author} You're not in a voice channel that I can see!`, 5, message)
      return false
    }

    if (!voiceChannel.members.array().includes(message.guild.me)) {
      await tempMessage(message.channel, `${message.author} You're not in a voice channel that I can see!`, 5, message)
      return false
    }

    try {
      await voiceChannel.leave()
      await tempMessage(message.channel, `${message.author} Left voice channel!`, 5, message)
    } catch (err) {
      await tempMessage(message.channel, `${message.author} I was unable to leave your voice channel.`, 5, message)
    }
  }
}
