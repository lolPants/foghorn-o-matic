const { Client, Message } = require('discord.js')  // eslint-disable-line
const Command = require('../Command')
const { tempMessage } = require('../helpers')
const { handleVoice } = require('../voiceHandler')

module.exports = class Join extends Command {
  constructor () {
    super('join', { guildOnly: true, admin: true })
  }

  /**
   * @param {Client} client Bot Client
   * @param {Message} message Instantiating Message
   * @param {string[]} args Command Arguments
   * @returns {*}
   */
  async run (client, message) {
    let voiceChannel = message.member.voice.channel
    if (!voiceChannel) {
      await tempMessage(message.channel, `${message.author} You're not in a voice channel that I can see!`, 5, message)
      return false
    }

    try {
      let connection = await voiceChannel.join()
      handleVoice(connection)
    } catch (err) {
      if (err.code === 'VOICE_JOIN_CHANNEL') {
        await tempMessage(message.channel, `${message.author} I don't have permission to join that voice channel!`, 5, message)
      } else {
        await tempMessage(message.channel, `${message.author} I was unable to join your voice channel!`, 5, message)
      }
      return false
    }
    await tempMessage(message.channel, `${message.author} Joined voice channel...`, 5, message)
  }
}
