const PERMISSIONS = [
  'CHANGE_NICKNAME',
  'VIEW_CHANNEL',
  'READ_MESSAGE_HISTORY',
  'SEND_MESSAGES',
  'MANAGE_MESSAGES',
  'CONNECT',
  'SPEAK',
  'USE_VAD',
]

const { Client, Message } = require('discord.js')  // eslint-disable-line
const Command = require('../Command')
const { tempMessage } = require('../helpers')

module.exports = class Invite extends Command {
  constructor () {
    super('invite')
  }

  /**
   * @param {Client} client Bot Client
   * @param {Message} message Instantiating Message
   * @param {string[]} args Command Arguments
   */
  async run (client, message) {
    try {
      let url = await client.generateInvite(PERMISSIONS)
      try {
        await message.author.send(`<${url}>`)

        if (message.guild !== null) await tempMessage(message.channel, `${message.author} Check your DMs!`, 5, message)
      } catch (err) {
        if (err.code === 50007) message.channel.send(`${message.author} <${url}>`)
      }
    } catch (err) {
      // No Op
    }
  }
}
