const tempMessage = async (channel, message, seconds = 5, parent = null) => {
  let toDelete = await channel.send(message)
  setTimeout(async () => {
    try {
      await toDelete.delete()
    } catch (err) {
      // No-op
    }

    if (parent) {
      try {
        await parent.delete()
      } catch (err) {
        // No-op
      }
    }
  }, seconds * 1000)
}

module.exports = { tempMessage }
