// Package Dependencies
const { Client, Message } = require('discord.js')  // eslint-disable-line
const client = new Client()

// Local Dependencies
const { prefixHandler, argumentSplitter } = require('./commandHandler')
const { tempMessage } = require('./helpers')
const Command = require('./Command') // eslint-disable-line

// Environment Variables
const { TOKEN, PREFIX } = process.env

/**
 * @type {Command[]}
 */
const registry = [
  require('./commands/join'),
  require('./commands/leave'),
  require('./commands/disconnect'),
  require('./commands/invite'),
].map(ImportedCommand => new ImportedCommand())

client.on('ready', () => {
  console.log('Foghorn-o-matic logged in!')
  for (let connection of client.voiceConnections.values()) {
    try {
      connection.disconnect()
    } catch (err) {
      // Ignore
    }
  }
})

client.on('message', message => {
  // Ignore Bots
  if (message.author.bot) return false

  // Test for Command
  let handled = prefixHandler(PREFIX, message.cleanContent, true)
  if (!handled) return false

  /**
   * @type {string}
   */
  let name = handled.name.toLowerCase()
  let args = argumentSplitter(handled.arguments)

  for (let command of registry) {
    if (command.name === name) {
      runCommand(message, command, args)
      break
    }
  }
})

/**
 * @param {Message} message Message that initiated this command
 * @param {Command} command Command to run
 * @param {string[]} args Command Arguments
 * @returns {*}
 */
const runCommand = async (message, command, args) => {
  let { owner } = await client.fetchApplication()

  let isOwner = message.author.id === owner.id
  let isAdmin = message.guild === null ? false : message.member.hasPermission('MOVE_MEMBERS')

  // Handle Command Flags
  if (command.guildOnly && (message.guild === null)) {
    await tempMessage(message.channel, 'This command cannot be used in Direct Messages!', 5, message)
    return false
  }

  if (command.nsfw && !message.channel.nsfw) {
    await tempMessage(message.channel, 'This command can only be ran in NSFW channels!', 5, message)
    return false
  }

  if (command.owner && !isOwner) {
    await tempMessage(message.channel, 'You do not have permission to run that command!', 5, message)
    return false
  }

  if (command.admin && !(isOwner || isAdmin)) {
    await tempMessage(message.channel, 'You do not have permission to run that command!', 5, message)
    return false
  }

  // Run command
  await command.run(client, message, args)
}

client.login(TOKEN)
