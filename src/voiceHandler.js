const { VoiceConnection } = require('discord.js') // eslint-disable-line
const path = require('path')

/**
 * @param {Object} obj Input
 * @returns {boolean}
 */
const checkSpeaking = obj => Object.values(obj).some(x => x === true)

/**
 * @param {VoiceConnection} connection Voice Connection
 */
const handleVoice = connection => {
  let isSpeaking = {}
  let isPlaying = false

  connection.on('speaking', (user, speaking) => {
    if (user.id !== connection.client.user.id) {
      isSpeaking[user.id] = speaking.has('SPEAKING')
    }

    if (checkSpeaking(isSpeaking) && !isPlaying) playFile()
  })

  const playFile = () => {
    const file = path.join(__dirname, 'audio', 'foghorn.wav')
    let dispatcher = connection.play(file)
    dispatcher.on('speaking', speaking => {
      isPlaying = speaking

      if (checkSpeaking(isSpeaking) && !isPlaying) playFile()
    })
  }
}

module.exports = { handleVoice }
