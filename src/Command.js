/**
 * @typedef {Object} Permissions
 * @property {boolean} guildOnly
 * @property {boolean} nsfw
 * @property {boolean} admin
 * @property {boolean} owner
 */

module.exports = class Command {
  constructor (name, permissions = { guildOnly: false, nsfw: false, admin: false, owner: false }) {
    /**
     * @type {string}
     */
    this._name = name.toLowerCase()

    /**
     * @type {Permissions}
     */
    this._permissions = permissions
  }

  get name () {
    return this._name.toLowerCase()
  }

  set name (name) {
    this._name = name.toLowerCase()
  }

  get guildOnly () {
    return this._permissions.guildOnly || false
  }

  get nsfw () {
    return this._permissions.nsfw || false
  }

  get admin () {
    return this._permissions.admin || false
  }

  get owner () {
    return this._permissions.owner || false
  }

  async run (client, message, args) { // eslint-disable-line
    // No op
  }
}
