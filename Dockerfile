# Alpine Node Image
FROM node:10-alpine

# Create app directory
WORKDIR /usr/app

# Copy package info
COPY package.json package-lock.json ./

# Install app dependencies
RUN apk add --no-cache ffmpeg bash git openssh make gcc g++ python tini && \
  npm i -g npm && \
  npm ci && \
  apk del bash git openssh make gcc g++ python

# Bundle app source
COPY . .

# Mount the directory with the .WAV
VOLUME [ "/usr/app/src/audio" ]

# Start Node.js
ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "node", "." ]
